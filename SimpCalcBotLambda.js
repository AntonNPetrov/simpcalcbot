/*
Copyright 2018 Anton Petrov

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

'use strict';

exports.handler = (event, context, callback) => {
//    console.log(`event: ${JSON.stringify(event)}\n` +
//        `context: ${JSON.stringify(context)}\n`);

    // Убедимся, что метод запроса - POST, и тело сообщения не пустое
    if( event.httpMethod !== "POST" || event.body == null )
        return callback(null, response({message: "Malformed request"}, 400));

    try {
        var body = JSON.parse(event.body);
//        console.log("event.body:\n" + JSON.stringify(body));
    } catch(e) {
        return callback(null, response({message: "Bad JSON-format data"}, 400));
    }
    if( body == null )
        return callback(null, response({message: "Malformed request"}, 400));

    var msg = body.message;
    var cbq = body.callback_query;

    if( msg != null && msg.text != null && msg.chat != null
            && Array.isArray(msg.entities) ) {
        // Обработка команд бота
        for(var el of msg.entities) {
            if(el.type !== "bot_command" ) continue;
            var command = msg.text.substr(el.offset+1, el.length).toLowerCase();
            if( command === "start" )
                var respBody = newCalculatorMessage(msg);
            else if( command === "help" )
                var respBody = helpMessage(msg);
            else continue;

            var resp = response(respBody);
//            console.log("response: " + JSON.stringify(resp));
            return callback(null, resp);
        }

        callback(null, {message: "Command not implemented"});
        return;
    } else if( cbq != null && cbq.data != null && cbq.message != null
            && cbq.message.chat != null )
    {
        // Обработка ввода с "клавиатуры"
        msg = cbq.message;
        // Жестко ограничим допустимую длину выражения
        if( msg.text == null || msg.text.length > 1024 )
            msg.text = "0";

        respBody = {
            method: "editMessageText",
            chat_id: msg.chat.id,
            message_id: msg.message_id,
            text: buttonPressed(msg.text, cbq.data)
        };
        if( cbq.data !== "PO" ) respBody.reply_markup = inlineKeyboard();

        var resp = response(respBody);
//        console.log("response: " + JSON.stringify(resp));
        return callback(null, resp);
    }

    var resp = response({message: "Ok"});
//    console.log("response: " + JSON.stringify(resp));
    callback(null, resp);
}

function response(bodyObj, statCode = 200) {
    return { body: JSON.stringify(bodyObj), statusCode: statCode };
}

function inlineKeyboard() {
    return { "inline_keyboard": [
        [ { "text": "AC", "callback_data": "AC" }, { "text": "+", "callback_data": "+" }, { "text": "-", "callback_data": "-" } ],
        [ { "text": "7", "callback_data": "7" }, { "text": "8", "callback_data": "8" }, { "text": "9", "callback_data": "9" } ],
        [ { "text": "4", "callback_data": "4" }, { "text": "5", "callback_data": "5" }, { "text": "6", "callback_data": "6" } ],
        [ { "text": "1", "callback_data": "1" }, { "text": "2", "callback_data": "2" }, { "text": "3", "callback_data": "3" } ],
        [ { "text": "PwrOff", "callback_data": "PO" }, { "text": "0", "callback_data": "0" }, { "text": "=", "callback_data": "=" } ]
    ]};
}

function newCalculatorMessage(msg) {
    return {
        method: "sendMessage",
        chat_id: msg.chat.id,
        text: "0",
        reply_markup: inlineKeyboard()
    };
}

function helpMessage(msg) {
    return {
        method: "sendMessage",
        chat_id: msg.chat.id,
        text: "Складывайте и вычитайте!"
    };
}

function buttonPressed(text, button) {
    if( button === "PO" ) return text;
    if( button === "AC" ) return "0";
    if( text === "Error" || text === "Overflow" ) text = "0";
    if( button === "=" ) {
        // Проверим, что в тексте выражения есть только цифры, + и -
        // Иначе сбросим текст в "0"
        if( !checkExpression(text) ) return "0";
        // Остальное предоставим eval-у
        try {
            text = eval(text).toString();
            // Это не защитит от ошибок вычислений с плавающей запятой,
            // но защитит от возврата некорректного выражения
            if( text.indexOf("e") !== -1 ) return "Overflow";
            return text;
        } catch(e) {
            return "Error";
        }
    }
    if( button >= "0" && button <= "9" || button === "+" || button === "-" ) {
        var fsm = new InputProcFSM();
        if( fsm.evaluate(text) === fsm.Error ) return "Error";
        if( fsm.transit(button) === fsm.Error ) return text;
        return (text === "0" && fsm.currState !== fsm.StartOfExpression)
            ? fsm.text : text+fsm.text;
    }
    return "Error";
}

function checkExpression(text) {
    for(var c of text)
        if( c !== "+" && c !== "-" && (c < "0" || c > "9") )
            return false;
    return true;
}

// Конечный автомат для коррекции пользовательского ввода
class InputProcFSM {
    constructor() {
        this.StartOfExpression = 0;
        this.WaitingFirstDigit = 1;
        this.MiddleOfNumber = 2;
        this.Error = 3;

        this.handlers = [
            this.startOfExpression, // 0
            this.waitingFirstDigit, // 1
            this.middleOfNumber // 2
        ];

        this.currState = this.StartOfExpression;
        this.text = "";
    }

    evaluate(str) {
        for(var c of str) {
            this.transit(c);
            if( this.currState === this.Error ) break;
        }
        return this.currState;
    }

    transit(input) {
        this.currState = this.handlers[this.currState].call(this, input);
        return this.currState;
    }

    startOfExpression(c) {
        if( c > "0" && c <= "9" ) { this.text = c; return this.MiddleOfNumber; }
        if( c === "-" ) { this.text = c; return this.WaitingFirstDigit; }
        if( c === "+" || c === "0" || c === "=" ) return this.StartOfExpression;
        return this.Error;
    }

    waitingFirstDigit(c) {
        if( c > "0" && c <= "9" ) { this.text = c; return this.MiddleOfNumber; }
        if( c === "0" || c === "+" || c === "-" ) { this.text = ""; return this.WaitingFirstDigit; }
        return this.Error;
    }

    middleOfNumber(c) {
        if( c >= "0" && c <= "9" ) { this.text = c; return this.MiddleOfNumber; }
        if( c === "+" || c === "-" ) { this.text = c; return this.WaitingFirstDigit; }
        return this.Error;
    }
}
