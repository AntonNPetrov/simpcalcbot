// Сравнение двух объектов в формате JSON
exports.deepCompare = (strA, strB) => {
    try {
        var a = JSON.parse(strA), b = JSON.parse(strB);
        return deepComp(a, b) && deepComp(b, a);
    } catch(e) {
        return false;
    }
}

function deepComp(a, b) {
    if(typeof a !== typeof b) return false;
    if(typeof a === 'object') {
        for(var name in a)
            if( !(b.hasOwnProperty(name) && deepComp(a[name], b[name])) )
                return false;
        return true;
    }
    else return a === b;
}
